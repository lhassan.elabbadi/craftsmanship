#include "ideebox.h"

static int Identifiant = 1;

void vider_stdin ()
{
   int c = getchar ();

   while (c != '\n' && c != EOF)
      c = getchar ();
}

int existId (Idee * list, int id)
{
   int res = 0;
   Idee *temp = list;

   while (temp != NULL && res != 1)
   {

      if (temp->identifiant == id)
      {
         res = 1;
      }
      else
      {
         temp = temp->suivante;
      }

   }

   return res;
}

char *Connection_idb (char *utilisateur)
{
   char login[UTIL_LEN];
   char domain[DOMAIN_LEN];
   char email[100];
   int c = 'O';

   utilisateur = (char *) malloc (EMAIL_LEN * sizeof (char));

   if (utilisateur == NULL)
   {
      printf ("Erreur : Pas de méoire alloue\n");
      return utilisateur;
   }
   memset (utilisateur, 0, EMAIL_LEN);
   memset (email, 0, EMAIL_LEN);
   scanf ("%s", email);

   while ((c != 'n' && c != 'N') &&
          sscanf (email, "%[_a-zA-Z0-9.]@%[_a-zA-Z0-9.]", login, domain) != 2)
   {
      printf ("Erreur : Email invalide: Voulez-vous continuer ? o/n \n");

      c = getchar ();
      if (c != 'n' || c != 'N')
      {
         printf ("Saisissez de nouveau votre mail: \n");
         memset (email, 0, EMAIL_LEN);
         scanf ("%s", email);
      }
      else
      {
         free (utilisateur);
         utilisateur = NULL;
      }
   }

   if (utilisateur)
   {
      strcpy (utilisateur, email);
   }

   return utilisateur;
}

int Faire_un_choix ()
{
   int choix = 0;
   int rtn = 0;

   printf ("Faites un choix: \n");
   printf ("1: Consulter les idees \n");
   printf ("2: Poster une idee \n");
   printf ("3: Modifier une idee \n");
   printf ("4: Supprimer une idee \n");
   printf ("5: Liker une idee \n");
   printf ("6: Unliker une idee \n");
   printf ("7: Quitter \n");
   rtn = scanf ("%d", &choix);

   if (rtn == EOF)
   {
      fputs ("(user canceled input.)\n", stderr);
      choix = 0;
      return -1;
   }
   else if (rtn == 0)
   {
      fputs (" error: invalid integer input.\n", stderr);
      vider_stdin ();
      choix = 0;
   }
   else if (choix < 1 || 8 < choix)
   {                            /* En dehors de l intervalle */
      fputs (" error: integer out of range [1-8]\n", stderr);
      vider_stdin ();
      choix = 0;
   }
   else
   {                            /* bonne entree */
      vider_stdin ();
   }

   return choix;

}

void Consulter (Idee * list_id)
{
   Idee *tmp = list_id;

   if (tmp == NULL)
   {
      printf ("La boite a idées est vide\n");
   }

   while (tmp != NULL)
   {
      /* On affiche */
      printf ("%d ", tmp->identifiant);
      printf ("%s ", tmp->proposant);
      printf ("%s ", tmp->idee);
      printf ("%d ", tmp->nb_likers);
      printf ("%d \n", tmp->nb_unlikers);

      tmp = tmp->suivante;
   }

   return;
}

void Modifier_idee (Idee * list)
{
   int id = 0;
   Idee *temp = list;
   char idee_txt[IDEE_LEN];
   int tem = 1;

   memset (idee_txt, 0, IDEE_LEN);

   if (list == NULL)
   {
      printf ("LIST NULL \n");
      return;
   }

   do
   {
      printf
         ("Donner l'identifiant de l'ide que vous voulez modifier ou taper 0 pour quitter\n");
      scanf ("%d", &id);
   }
   while (!existId (list, id) && (id != 0));

   if (id == 0)
   {
      printf ("Quitter sans modifier\n");
      return;
   }

   /* flush the input buffer */
   vider_stdin ();

   temp = list;
   while (temp != NULL && tem == 1)
   {
      if (temp->identifiant == id)
      {
         fgets (idee_txt, IDEE_LEN, stdin);

         if ((strlen (idee_txt) > 0)
             && (idee_txt[strlen (idee_txt) - 1] == '\n'))
         {
            idee_txt[strlen (idee_txt) - 1] = '\0';
         }

         memset (temp->idee, 0, IDEE_LEN);
         strcpy (temp->idee, idee_txt);
         tem = 0;
      }
      else
      {
         temp = temp->suivante;
      }
   }
}

int Supprimer_idee (Idee ** list)
{
   int id = 0;
   int res = 0;
   Idee *tmp = NULL;
   Idee *tmp1 = NULL;

   if (*list == NULL)
   {
      printf ("Liste vide! Rien  supprimer\n");
      return res;
   }

   do
   {
      printf
         ("Donner l'identifiant de l'ide que vous voulez supprimer ou taper 0 pour quitter\n");
      scanf ("%d", &id);
   }
   while (!existId (*list, id) && (id != 0));

   if (id == 0)
   {
      printf ("Quitter sans rien supprimer\n");
      return res;
   }

   /* Traitement du cas particulier tete de liste */
   if ((*list)->identifiant == id)
   {
      tmp = *list;
      *list = (*list)->suivante;
      free (tmp);
      printf ("Element id = %d est supprimé\n", id);
      res = 1;
   }
   else
   {
      tmp = *list;

      while (tmp->suivante && tmp->suivante->identifiant != id)
      {
         tmp = tmp->suivante;
      }

      if (tmp->suivante)
      {
         tmp1 = tmp->suivante;
         tmp->suivante = tmp1->suivante;
         free (tmp1);
         printf ("Element id = %d est supprimé\n", id);
         res = 1;
      }
   }

   /* On met a jour les identifiant */
   Identifiant = 1;
   tmp = *list;

   while (tmp != NULL)
   {
      tmp->identifiant = Identifiant++;
      tmp = tmp->suivante;
   }

   return res;
}

void Liker_idee (Idee * list, int like)
{
   int id = 0;
   int tem = 0;
   Idee *tmp = list;

   do
   {
      if (like == 1)
      {
         printf
            ("Donner l'identifiant de l'idee que vous voulez liker, 0 pour quitter\n");
      }
      else
      {
         printf
            ("Donner l'identifiant de l'idee que vous voulez disliker, 0 pour quitter\n");
      }
      scanf ("%d", &id);
   }
   while (!existId (list, id) && (id != 0));

   if (id == 0)
   {
      printf ("Quitter sans liker ou disliker\n");
      return;
   }

   while ((tmp != NULL) && (tem != 1))
   {
      if (tmp->identifiant == id)
      {
         if (like > 0)
         {
            tmp->nb_likers++;
            tem = 1;
         }
         else
         {
            tmp->nb_unlikers++;
            tem = 1;
         }
      }
      else
      {
         tmp = tmp->suivante;
      }
   }

}

Idee *Poster_idee (Idee ** list, char *utilisateur)
{
   char idee_txt[IDEE_LEN];
   Idee *temp = *list;
   Idee *nouvel_idee = (Idee *) malloc (sizeof (Idee));

   memset (idee_txt, 0, IDEE_LEN);

   printf ("Poster votre idee:\n");

   fgets (idee_txt, IDEE_LEN, stdin);
   nouvel_idee->identifiant = Identifiant++;
   strcpy (nouvel_idee->proposant, utilisateur);
   strcpy (nouvel_idee->idee, idee_txt);
   nouvel_idee->nb_likers = 0;
   nouvel_idee->nb_unlikers = 0;
   nouvel_idee->suivante = NULL;

   if (*list == NULL)
   {
      *list = nouvel_idee;
      return *list;
   }
   else
   {
      while (temp->suivante != NULL)
      {
         temp = temp->suivante;
      }
      temp->suivante = nouvel_idee;

      return *list;

   }
}

Idee *Poster_idee_ (Idee * list, char *utilisateur, char *idee_tx)
{
   char idee_txt[IDEE_LEN];
   Idee *nouvel_idee = (Idee *) malloc (sizeof (Idee));

   memset (idee_txt, 0, IDEE_LEN);

   strcpy (idee_txt, idee_tx);

   nouvel_idee->identifiant = Identifiant++;
   strcpy (nouvel_idee->proposant, utilisateur);
   strcpy (nouvel_idee->idee, idee_txt);
   nouvel_idee->nb_likers = 0;
   nouvel_idee->nb_unlikers = 0;
   nouvel_idee->suivante = NULL;

   if (list == NULL)
   {
      list = nouvel_idee;
      return nouvel_idee;
   }
   else
   {
      Idee *temp = list;
      while (temp->suivante != NULL)
      {
         temp = temp->suivante;
      }
      temp->suivante = nouvel_idee;
      return list;

   }
}

Idee *Charger_list (Idee * list)
{
   char utilisateur1[20];
   char utilisateur2[20];
   char utilisateur3[20];

   char str_1[20];
   char str_2[20];
   char str_3[20];

   strcpy (utilisateur1, "Hassan@yahoo.fr");
   strcpy (utilisateur2, "Ali@yahoo.com");
   strcpy (utilisateur3, "Said@protonmail.es");
   strcpy (str_1, "Ya Salam");
   strcpy (str_2, "Bueons dias");
   strcpy (str_3, "Hola Las palmas");

   list = Poster_idee_ (list, utilisateur1, str_1);
   list = Poster_idee_ (list, utilisateur2, str_2);
   list = Poster_idee_ (list, utilisateur3, str_3);

   return list;
}
