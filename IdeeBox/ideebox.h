#ifndef H_IDEEBOX
#define H_IDEEBOX

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UTIL_LEN 50
#define DOMAIN_LEN 50
#define EMAIL_LEN 100
#define IDEE_LEN 200

typedef struct List_like
{
   char liker_mail[EMAIL_LEN];
   struct List_like *suivant;
} List_like;

typedef struct Idee
{
   int identifiant;
   char proposant[EMAIL_LEN];
   char idee[IDEE_LEN];
   struct List_like *list_likers;
   struct List_like *list_unlikers;
   int nb_likers;
   int nb_unlikers;
   struct Idee *suivante;
} Idee;

struct Idee *list_idee;

int existId (Idee * list, int id);

char *Connection_idb (char *utilisateur);

int Faire_un_choix ();

void Consulter (Idee * list_id);

void Modifier_idee (Idee * list);

int Supprimer_idee (Idee ** list);

void Liker_idee (Idee * list, int like);

Idee *Poster_idee (Idee ** list, char *utilisateur);

Idee *Poster_idee_ (Idee * list, char *utilisateur, char *idee_tx);

Idee *Charger_list (Idee * list);

#endif
