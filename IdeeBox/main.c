#include "ideebox.h"

int main ()
{
   int choix = 0;
   int quit = 0;
   Idee *list_id = NULL;
   char *utilisateur = NULL;
   list_id = Charger_list (list_id);
   printf ("Connecter-vous via votre mail: \n");

   utilisateur = Connection_idb (utilisateur);

   if (utilisateur)
   {
      do
      {
         choix = Faire_un_choix ();
         switch (choix)
         {
         case 1:
            Consulter (list_id);
            break;
         case 2:
            Poster_idee (&list_id, utilisateur);
            Consulter (list_id);
            break;
         case 3:
            Modifier_idee (list_id);
            Consulter (list_id);
            break;
         case 4:
            Supprimer_idee (list_id);
            Consulter (list_id);
            break;
         case 5:
            Liker_idee (list_id, 1);
            Consulter (list_id);
            break;
         case 6:
            Liker_idee (list_id, -1);
            Consulter (list_id);
            break;
         case 7:
            quit = 1;
            break;
         default:
            printf ("Choix invalide\n");
            break;
         }
      }
      while (quit == 0);

   }
   else
   {
      printf ("Au revoir!\n");
   }
   if (utilisateur)
   {
      free (utilisateur);
   }
   return 0;
}
